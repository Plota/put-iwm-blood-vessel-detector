import statistics
import sys

import skimage
import os
import numpy as np
import matplotlib.pyplot as plt
import skimage.measure
import cv2
from scipy._lib.six import xrange
from random import randint
from sklearn import ensemble
from sklearn.svm import SVC


def compute(img,raw_img):
    # result = img.ravel()
    result = [statistics.variance(img.ravel())]
    raw_m = skimage.measure.moments(img)
    result = np.concatenate((result, [statistics.variance(raw_img.ravel())]))
    if raw_m[0,0] != 0.0:
        cr = raw_m[0, 1] / raw_m[0, 0]
        cc = raw_m[1, 0] / raw_m[0, 0]
        central_m = skimage.measure.moments_central(img, center=(cr, cc), order=3)
        norm_m = skimage.measure.moments_normalized(central_m)
        hu = skimage.measure.moments_hu(norm_m)
    else:
        central_m = np.zeros(shape=(4,4))
        hu = np.zeros(shape=(1,7))

    tmp = central_m.ravel()
    result = np.concatenate((result, tmp))
    tmp = hu.ravel()
    result = np.concatenate((result, tmp))

    for i in range(len(result)):
        if not np.isfinite(result[i]):
            result[i] = 0

    return result


def process_image(img):
    kernel = np.ones((5,5),np.uint8)
    resultImage = cv2.erode(img, kernel, iterations=1)
    kernel = np.ones((3,3),np.uint8)
    opening = cv2.morphologyEx(resultImage, cv2.MORPH_OPEN, kernel)
    kernel = np.ones((5,5),np.uint8)
    closing = cv2.morphologyEx(opening, cv2.MORPH_CLOSE, kernel)
    edges = cv2.Canny(closing, 50, 150, apertureSize=3)
    return edges


def build():
    b_images = []
    b_processed = []
    b_detecteds = []
    input_images = os.listdir("scan")
    input_detected = os.listdir("hand")
    for i in range(len(input_images)):
        img = cv2.imread("scan/" + input_images[i], cv2.IMREAD_GRAYSCALE)
        clahe = cv2.createCLAHE(clipLimit=3, tileGridSize=(15, 15))
        img = clahe.apply(img)
        kernel = np.ones((10, 10), np.uint8)
        opening = cv2.morphologyEx(img, cv2.MORPH_OPEN, kernel)
        th3 = cv2.adaptiveThreshold(opening, 255, cv2.ADAPTIVE_THRESH_MEAN_C, \
                                    cv2.THRESH_BINARY, 11, 2)
        b_images.append(th3)
        img = process_image(img)
        b_processed.append(img)
        detected = cv2.imread("hand/" + input_detected[i], cv2.IMREAD_GRAYSCALE)
        b_detecteds.append(detected)
    return b_images, b_processed, b_detecteds


def train(images, procesed_images, detecteds):
    x_train = []
    y_train = []
    for i in range(1,15):
        good = 0
        bad = 0
        img = images[i]
        proc = procesed_images[i]
        detected = detecteds[i]
        while bad < 100:
            x = randint(0, proc.shape[0] - 5)
            y = randint(0, proc.shape[1] - 5)
            if detected[x][y] == 0:
                mask = np.zeros(shape=(9, 9))
                proc_mask = np.zeros(shape=(9, 9))
                for j in range(-4, 5):
                    for k in range(-4, 5):
                        mask[j + 4][k + 4] = img[x + j][y + k]
                        proc_mask[j + 4][k + 4] = proc[x + j][y + k]
                tmp = compute(proc_mask,mask)
                x_train.append(tmp)
                y_train.append(0)
                bad = bad + 1
        while good < 200:
            x = randint(0, proc.shape[0] - 5)
            y = randint(0, proc.shape[1] - 5)
            if detected[x][y] == 255:
                mask = np.zeros(shape=(9, 9))
                proc_mask = np.zeros(shape=(9, 9))
                for j in range(-4, 5):
                    for k in range(-4, 5):
                        mask[j + 4][k + 4] = img[x + j][y + k]
                        proc_mask[j + 4][k + 4] = proc[x + j][y + k]
                tmp = compute(proc_mask,mask)
                x_train.append(tmp)
                y_train.append(1)
                good = good + 1
    x_train = np.array(x_train)
    y_train = np.array(y_train)
    print("zakończono naukę")
    return x_train, y_train


def test(clf,images, procesed_images, detecteds):
    true_positive = 0
    true_negative = 0
    false_positive = 0
    false_negative = 0

    for i in range(15, 20):
        good = 0
        bad = 0
        img = images[i]
        detected = detecteds[i]
        proc = procesed_images[i]
        while good < 100:
            x = randint(0, detected.shape[0] - 5)
            y = randint(0, detected.shape[1] - 5)
            if detected[x][y] == 255:
                mask = np.zeros(shape=(9, 9))
                proc_mask = np.zeros(shape=(9, 9))
                for j in range(-4, 5):
                    for k in range(-4, 5):
                        mask[j + 4][k + 4] = img[x + j][y + k]
                        proc_mask[j + 4][k + 4] = proc[x + j][y + k]
                a = clf.predict([compute(proc_mask,mask)])
                decision = a[0]

                if detected[x][y] != 0:
                    if decision != 0:
                        true_positive += 1
                    elif decision == 0:
                        false_negative += 1
                elif detected[x][y] == 0:
                    if decision == 0:
                        true_negative += 1
                    elif decision != 0:
                        false_positive += 1
                good = good + 1
        while bad < 100:
            x = randint(0, detected.shape[0] - 5)
            y = randint(0, detected.shape[1] - 5)
            if detected[x][y] == 0:
                mask = np.zeros(shape=(9, 9))
                proc_mask = np.zeros(shape=(9, 9))
                for j in range(-4, 5):
                    for k in range(-4, 5):
                        mask[j + 4][k + 4] = img[x + j][y + k]
                        proc_mask[j + 4][k + 4] = proc[x + j][y + k]
                a = clf.predict([compute(proc_mask,mask)])
                decision = a[0]

                if detected[x][y] != 0:
                    if decision != 0:
                        true_positive += 1
                    elif decision == 0:
                        false_negative += 1
                elif detected[x][y] == 0:
                    if decision == 0:
                        true_negative += 1
                    elif decision != 0:
                        false_positive += 1
                bad = bad + 1

    accuracy = (true_positive + true_negative) / (
            true_positive + true_negative + false_negative + false_positive)
    sensitivity = true_positive / (true_positive + false_negative)
    specificity = true_negative / (true_negative + false_positive)

    print("TP = " + str(true_positive) + " | TN = " + str(true_negative))
    print("FP = " + str(false_positive) + " | FN = " + str(false_negative))
    print("accuracy = " + str(accuracy))
    print("sensitivity = " + str(sensitivity))
    print("specificity = " + str(specificity))


def prediction(clf, input_image,end_detected):
    detected = end_detected
    copy = input_image
    copy_for_3 = input_image
    true_positive = 0
    true_negative = 0
    false_positive = 0
    false_negative = 0
    c = 0
    res = np.zeros(shape=(input_image.shape[0], input_image.shape[1]))
    clahe = cv2.createCLAHE(clipLimit=3, tileGridSize=(15, 15))
    input_image = clahe.apply(input_image)
    copy2 = input_image
    proc_image = process_image(input_image)
    
    for x in xrange(input_image.shape[0] - 10):
        for y in xrange(input_image.shape[1] - 10):
            c = c + 1
            mask = np.zeros(shape=(9, 9))
            proc_mask = np.zeros(shape=(9, 9))
            for j2 in range(-4, 5):
                for k2 in range(-4, 5):
                    mask[j2 + 4][k2 + 4] = input_image[x + j2][y + k2]
                    proc_mask[j2 + 4][k2 + 4] = proc_image[x + j2][y + k2]

            tmp = compute(proc_mask,mask)
            result = clf.predict([tmp])
            res[x][y] = result[0]
            if result[0] == 1:
                copy[x][y] = 255
            if c % 20500 == 0:
                print(str(c/4100) + "%")

            if detected[x][y] != 0:
                if result[0] != 0:
                    true_positive += 1
                elif result[0] == 0:
                    false_negative += 1
            elif detected[x][y] == 0:
                if result[0] == 0:
                    true_negative += 1
                elif result[0] != 0:
                    false_positive += 1
    accuracy = (true_positive + true_negative) / (
            true_positive + true_negative + false_negative + false_positive)
    sensitivity = true_positive / (true_positive + false_negative)
    specificity = true_negative / (true_negative + false_positive)
    print("przetwarzanie na 4")
    print("TP = " + str(true_positive) + " | TN = " + str(true_negative))
    print("FP = " + str(false_positive) + " | FN = " + str(false_negative))
    print("accuracy = " + str(accuracy))
    print("sensitivity = " + str(sensitivity))
    print("specificity = " + str(specificity))                   
  
    fig, axes = plt.subplots(2, 2, figsize=(30, 60))
    axes[0,0].imshow(res, cmap='gray')
    axes[0,1].imshow(copy, cmap='gray')

    true_positive = 0
    true_negative = 0
    false_positive = 0
    false_negative = 0       
        
         
    copy2 = process_image(copy2)
    th3 = cv2.adaptiveThreshold(copy2, 255, cv2.ADAPTIVE_THRESH_MEAN_C, \
                                    cv2.THRESH_BINARY, 11, 2)
    for x in xrange(input_image.shape[0] - 10):
        for y in xrange(input_image.shape[1] - 10):
            if th3[x][y] == 0:
                copy_for_3[x][y] = 255
                
            
            if detected[x][y] != 0:
                if th3[x][y] == 0:
                    true_positive += 1
                elif th3[x][y] != 0:
                    false_negative += 1
            elif detected[x][y] == 0:
                if th3[x][y] != 0:
                    true_negative += 1
                elif th3[x][y] == 0:
                    false_positive += 1
        accuracy = (true_positive + true_negative) / (
            true_positive + true_negative + false_negative + false_positive)
    sensitivity = true_positive / (true_positive + false_negative)
    specificity = true_negative / (true_negative + false_positive)
    print("przetwarzanie na 3")
    print("TP = " + str(true_positive) + " | TN = " + str(true_negative))
    print("FP = " + str(false_positive) + " | FN = " + str(false_negative))
    print("accuracy = " + str(accuracy))
    print("sensitivity = " + str(sensitivity))
    print("specificity = " + str(specificity))
    
    

    axes[1,0].imshow(th3, cmap='gray')
    axes[1,1].imshow(copy_for_3, cmap='gray')
    plt.axis('off')
    plt.savefig('a.png')
    plt.show()


